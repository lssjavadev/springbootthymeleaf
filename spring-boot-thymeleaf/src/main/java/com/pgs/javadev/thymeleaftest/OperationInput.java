package com.pgs.javadev.thymeleaftest;

public class OperationInput {
	private String a;
	private String b;
	private String operation;
	
	public OperationInput() {
		a = "7";
		b = "3";
		operation = "+";
	}

	public String getA() {
		return a;
	}

	public void setA(String a) {
		this.a = a;
	}

	public String getB() {
		return b;
	}

	public void setB(String b) {
		this.b = b;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public OperationType getOperationType() {
		Character o;
		
		if(operation.length() > 1)
			return OperationType.UNKNOWN;
		
		try {
			o = operation.charAt(0);
		} catch(IndexOutOfBoundsException e) {
			return OperationType.UNKNOWN;
		}
		
		switch (o) {
			case '+':
				return OperationType.ADD;
				
			case '-':
				return OperationType.SUB;
				
			case '*':
				return OperationType.MULT;
				
			case '/':
				return OperationType.DIV;
				
			case '^':
				return OperationType.POW;
	
			default:
				return OperationType.UNKNOWN;
		}
	}
}
