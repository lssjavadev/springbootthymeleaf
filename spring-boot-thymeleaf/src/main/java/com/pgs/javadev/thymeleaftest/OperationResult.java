package com.pgs.javadev.thymeleaftest;

public class OperationResult {
	private Integer result;
	private OperationType operation;
	private OperationResultType resultType;
	
	public OperationResult() {
		result = 42;
		operation = OperationType.UNKNOWN;
		resultType = OperationResultType.UNKNOWN;
	}

	public void calculate(OperationType operation, Integer a, Integer b) {
		setOperation(operation);

		switch (getOperation()) {
			case ADD:
				setResult(a + b);
				setResultType(OperationResultType.OK);
				break;
	
			case SUB:
				setResult(a - b);
				setResultType(OperationResultType.OK);
				break;
	
			case MULT:
				setResult(a * b);
				setResultType(OperationResultType.OK);
				break;
	
			case DIV:
				if (b != 0) {
					setResult(a / b);
					setResultType(OperationResultType.OK);
				} else {
					setResult(0);
					setResultType(OperationResultType.ERROR);
				}
				break;
	
			case POW:
				setResult((int)Math.pow(a, b));
				setResultType(OperationResultType.OK);
				break;
	
			default:
				setResult(0);
				setResultType(OperationResultType.ERROR);
				break;
		}
	}

	public OperationResultType getResultType() {
		return resultType;
	}

	public void setResultType(OperationResultType resultType) {
		this.resultType = resultType;
	}

	public Integer getResult() {
		return result;
	}

	public void setResult(Integer result) {
		this.result = result;
	}

	public OperationType getOperation() {
		return operation;
	}

	public void setOperation(OperationType operation) {
		this.operation = operation;
	}

}
