package com.pgs.javadev.thymeleaftest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ThymeLeafController {

	@RequestMapping("/")
	String index() {
		return "index";
	}

	@RequestMapping(value = "/calculator", method = RequestMethod.GET)
	String calculator(Model model) {
		OperationInput calc = new OperationInput();
		OperationResult calcResult = new OperationResult();

		model.addAttribute("calc", calc);
		model.addAttribute("calcResult", calcResult);

		return "calculator";
	}

	@RequestMapping(value = "/calculator", method = RequestMethod.POST)
	String calculator(Model model, @ModelAttribute(value = "calc") OperationInput calc) {
		OperationResult calcResult = new OperationResult();

		try {
			calcResult.calculate(calc.getOperationType(), Integer.parseInt(calc.getA()), Integer.parseInt(calc.getB()));
		} catch (NumberFormatException e) {
			calcResult.calculate(OperationType.ERROR, 0, 0);
		}

		model.addAttribute("calcResult", calcResult);

		return "calculator";
	}
	
}
