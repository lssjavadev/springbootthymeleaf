package com.pgs.javadev.thymeleaftest;

public enum OperationType {
	ADD,
	SUB,
	DIV,
	MULT,
	POW,
	ERROR,
	UNKNOWN
}
