package com.pgs.javadev.thymeleaftest;

public enum OperationResultType {
	OK,
	ERROR,
	UNKNOWN
}
